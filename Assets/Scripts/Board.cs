﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;



public class Board : MonoBehaviour
{
    private GameObject[,] arrayWall = new GameObject[6, 6];
    private GameObject[,] arrayFloor = new GameObject[6, 6];

    public GameObject player;
    public GameObject[] typeTitle;

    private int[,,] mask;
    public static int _lvlCount;

    public void Start()
    {
        mask = GetComponent<LevelMasks>().mask;
        CreaterLevel(LvlManager.lvl);
    }

    public void CreaterLevel(int lvl)
    {
        _lvlCount = lvl;
        CreateBorder();
    }

    public void Destroyer()
    {
       
        while (transform.GetChild(0) != null)
        {
            Destroy(transform.GetChild(0).gameObject);
        }
        
    }

    public void GetArrayWall(int x, int y)
    {
        arrayWall[x, y] = Instantiate(typeTitle[0], new Vector3(x, y, -1), Quaternion.identity);
        arrayWall[x, y].transform.parent = gameObject.transform;
    }

    public void GetArrayFloor(int x, int y, bool isPainted)
    {
        arrayFloor[x, y] = Instantiate(typeTitle[1], new Vector3(x, y, 0), Quaternion.identity);
        GameObject obj = arrayFloor[x, y];
        obj.transform.parent = gameObject.transform;
        LvlManager.countFloorNotPainted++;
        if (isPainted)
            obj.GetComponent<TileFloor>().PaintedTile();
    }

    public void CreateBorder()
    {
        for (int i = 0; i < mask.GetLength(1); i++)
        {
            for (int j = 0; j < mask.GetLength(2); j++)
            {
                switch (mask[_lvlCount, j, i])
                {
                    case 0:
                        GetArrayWall(i, j);
                        break;
                    case 1:
                        GetArrayFloor(i, j, false);
                        break;
                    case 3:
                        GetArrayFloor(i, j, true);
                        player.GetComponent<Player>().MovePlayerToStart(new Vector3(i, j, -1));
                        break;
                }
            }
        }
    }

}
