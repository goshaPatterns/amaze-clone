﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public  class LvlManager : MonoBehaviour
{
    public static int countFloor;
    public static int countFloorNotPainted;
    public static int lvl;

    public GameObject boardObj;
    private Board _board;

    public void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (countFloorNotPainted == 0)
        {
            if (lvl < 2)
                lvl++;
            else
                lvl = 0;

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            
        }
            
    }
    
}
