﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Transform _transform;
    private Vector3 _toward;
    private Vector3 _position;
    private float _distance;
    private bool _move;
    private Vector2 dir;
    private PlayerAnim anim;

    private void Start()
    {
        _transform = transform;
        anim = GetComponent<PlayerAnim>();
    }

    private void Update()
    {
        if (_move)
        {
            MovePlayerToToward();
            PaintedTile();
        }
    }

    public void MovePlayerToStart(Vector3 pos)
    {
        _transform.position = pos;
    }

    public void MovePlayerToToward()
    {
        _distance = Vector3.Distance(transform.position, _toward);
        
        float step = 20 * Time.deltaTime;
        
        if (_distance != 0)
            _transform.position = Vector3.MoveTowards( _transform.position, _toward, step);
        else
        {
            _move = false;
            anim.PlayAnim(dir);
        }
    }

    public void SetTowars(Vector2 pos, Vector2 dir)
    {
        Vector2 reuslt = pos - dir;
        _toward =  new Vector3(reuslt.x, reuslt.y, -1 );
        _move = true;
        this.dir = dir;
    }
    
    public void PlayerMove(Vector2 dir)
    {
        if (!_move)
        {
            Ray CheckRay = new Ray(_transform.position, dir*10);
            RaycastHit CheckHit;
            if (Physics.Raycast(CheckRay, out CheckHit, 10))
            {
                if (CheckHit.collider != null)
                {
                    GameObject wall = CheckHit.collider.gameObject;
                    SetTowars(wall.GetComponent<TileWall>().SetCoordinates(), dir);
                }
            }
        }
    }

    public void PaintedTile()
    {
        Ray PaintRay = new Ray(_transform.position, Vector3.forward);
        RaycastHit PaintHit;
        if (Physics.Raycast(PaintRay, out PaintHit, 1))
        {
            if (PaintHit.collider != null)
            {
                GameObject floor = PaintHit.collider.gameObject;
                floor.GetComponent<TileFloor>().PaintedTile();
            }
        }
    }
    
    
    
}
