﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnim : MonoBehaviour
{
    private Animator anim;
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(0.3f);
        anim.SetBool("Plush", false);
        anim.SetBool("Vert", false);
    }

    public void PlayAnim(Vector2 dir)
    {
        if (dir.x != 0)
        {
            anim.SetBool("Plush", true);
            StartCoroutine(wait());
        }
        else if (dir.y != 0)
        {
            anim.SetBool("Vert", true);
            anim.SetBool("Plush", true);
            StartCoroutine(wait());

        }
    }
}    


