﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PushButtonEvent : MonoBehaviour, IBeginDragHandler, IDragHandler
{
    public GameObject playerObj;
    private Player player;
    public GameObject boardObj;
    private Board _board;
    
    public Vector2 startPos;
    public Vector2 direction;
    public bool directionChosen;
    
    private void Start()
    {
        player = playerObj.GetComponent<Player>();
        _board = boardObj.GetComponent<Board>();
    }

    void Update()
    {

        if (Input.GetKey(KeyCode.UpArrow))
        {
            player.PlayerMove(Vector2.up);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            player.PlayerMove(-Vector2.up);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            player.PlayerMove(Vector2.right);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            player.PlayerMove(-Vector2.right);
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if((Math.Abs(eventData.delta.x))>(Math.Abs(eventData.delta.y)))
        {
            if (eventData.delta.x > 0)
                player.PlayerMove(Vector2.right);
            else
                player.PlayerMove(-Vector2.right);
        }
        else
        {
            if(eventData.delta.y>0)
                player.PlayerMove(Vector2.up);
            else
                player.PlayerMove(-Vector2.up);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        
    }
}
