﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileFloor : MonoBehaviour
{

    public bool isPainted;
    public Material paint;

    
    public void PaintedTile()
    {
        if (!isPainted)
        {
            GetComponent<MeshRenderer>().material = paint;
            isPainted = true;
            if(LvlManager.countFloorNotPainted>0) 
                LvlManager.countFloorNotPainted--;
        }
    }
}
