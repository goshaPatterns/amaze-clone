﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileWall : MonoBehaviour
{
    public float x;
    public float y;
    void Start()
    {
        x = transform.position.x;
        y = transform.position.y;
    }

    public Vector2 SetCoordinates()
    {
        return new Vector2(x,y);
    }
    
}
